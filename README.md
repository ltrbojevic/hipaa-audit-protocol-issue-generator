# HIPAA Audit Protocol Issue Generator

This simple script creates an issue for each audit inquiry in the HIPAA Audit Protocol [published](https://www.hhs.gov/hipaa/for-professionals/compliance-enforcement/audit/protocol/index.html) by the U.S. Department of Health & Human Services. Note this tool doesn't guarantee compliance with any regulation or successful completion of an audit. There are also no guarantees the data in the CSV are accurate or up-to-date, as it's subject to change by HHS at any time.

The purpose of this script is to demonstrate the capability of the GitLab API in creating project workflows within GitLab.

# Installation & Use

## Install python-gitlab

This script uses `python-gitlab`, an unofficial Python wrapper for the GitLab API. There's no particular reason this wrapper was selected over others. Its repository is [here](https://github.com/python-gitlab/python-gitlab).

The easiest way to install is with pip:
`pip install python-gitlab`

## Running the script

1. Make sure `python-gitlab` is installed (see above)
1. If you haven't already, create a new project for the issues to be created in. Be aware running this script will create **180** issues, so a new project is recommended
1. On GitLab, create a new Personal Access Token with the `api` scope. You can do this on the [Access Tokens](https://gitlab.com/profile/personal_access_tokens) page.
1. Open `issue_generator.py` add your Personal Access Token to `private_token=''` on line 9 and your project's ID to `project_id =''` on line 12
1. Run the script with `python issue_generator.py` (make sure the csv and script are in the same folder; if they're not, adjust the path to the csv in `issue_generator.py`)
1. It may take several minutes for the script to complete generating the issues. If you cancel the script before it's completed, there's no way to resume issue creation other than to find the last issue created and modify the CSV to only include the audit inquiries after it

If you're using a self-hosted GitLab instance, you can just point to your domain/IP by changing the `gitlab.Gitlab` object. More on that in the `python-gitlab` [documentation page](https://python-gitlab.readthedocs.io/en/stable/api-usage.html).

## Structure of the issues

* The title of the issue is the Key Activity
* The issue description contains the Established Performance Criteria and Audit Inquiry, which are separated into their own headers
* Every issue has up to three labels: Audit Type (Privacy, Security, Breach), Required/Addressable, and Section (the reference to the regulation)

## How to work with the issues

There's no right or wrong way to use these issues. Every organization's workflows and requirements will be different, so find what works best for you. You can use issue discussions to upload files, link to resources, and have discussions, but you can also use any of GitLab's tools offered at your subscription level to create whatever workflow you'd like.
